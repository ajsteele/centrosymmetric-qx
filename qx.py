import numpy as np

REDUCED_FACTORIZATION = 'reduced'
COMPLETE_FACTORIZATION= 'complete'

# This default is based on np.linalg.qr
DEFAULT_FACTORIZATION = REDUCED_FACTORIZATION

TOL = 100*np.finfo(np.double).eps

# NUMERICAL CONSTANTS

SQRT2 = np.sqrt(2)

# Errors
class Error(Exception):
  """Base class for exceptions"""
  pass

class CentrosymmetryError(Error):
  """
    Exception raised for non-centrosymmetric matrices
  """
  pass

class ReducedDimensionError(Error):
  """
    Exception raised for reduced factoriztion with m < n
  """
  pass


def get_sub_qr(B,mode=DEFAULT_FACTORIZATION):
  """
    Computes the `QR` factorization of submatrix `B` and ensures
    positive diagonal coefficients in double-cone matrix `X`.

  """
  Q,R = np.linalg.qr(np.matrix(B),mode=mode)
  ind = np.where(np.diag(R) < 0)[0]
  Q[:,ind] = -Q[:,ind]
  R[ind,:] = -R[ind,:]
  return Q,R

def qx(A,mode=DEFAULT_FACTORIZATION,rtol=TOL,atol=TOL):
  """
  =====================================================================
  qx  -- Compute QX factorization of a real centrosymmetric matrix `A`.
  ---------------------------------------------------------------------

  Factor the real centrosymmetric matrix `A` as `QX`, where `Q` is
  orthonormal and centrosymmetric and `X` is a centrosymmetric
  double-cone matrix.


  Parameters
  ----------
  A : array_like, float, shape (m,n)
    Real centrosymmetric matrix
  mode : 

  Returns
  -------
  Q : array_like, float, shape (m,m)
    Real centrosymmetric orthogonal matrix
  X : array_like, float, shape (m,n)
    Real centrosymmetric double-cone matrix

  Raises
  ------
  ReducedDimensionError
    If mode='reduced' and `m<n`
  CentrosymmetryError
    If `A != JAJ` where `J` is the exchange matrix.

  References
  ----------

  [1]: https://doi.org/10.1016/j.laa.2015.06.036
  [2]: https://en.wikipedia.org/wiki/Centrosymmetric_matrix
  [3]: https://en.wikipedia.org/wiki/QR_decomposition
  [4]: https://en.wikipedia.org/wiki/Orthogonal_matrix
  [5]: https://en.wikipedia.org/wiki/Exchange_matrix

  """

  # ====================================================================
  # NOTE: This code takes advantage of empty block matrices. 
  # ====================================================================

  m,n = A.shape

  if mode == REDUCED_FACTORIZATION and m < n:
    raise ReducedDimensionError('Reduced factorization requires m >= n')

  if not np.allclose(A,A[::-1,::-1],rtol=rtol,atol=atol):
    raise CentrosymmetryError('Input matrix A must be centrosymmetric')

  p1 = m//2 
  q1 = n//2
  p2 = np.ceil(m/2).astype(int)  
  q2 = np.ceil(n/2).astype(int)
  p12= np.arange(p1,p2)
  q12= np.arange(q1,q2)
  dp = m&1
  dq = n&1
  A1 = A[:p1,:q1]
  A2 = A[:p1,q2:][:,::-1]
  u  = A[:p1,q12]
  v  = A[p12,:q1]
  a  = A[p12,q12].reshape(dp,dq)
  B1 = np.block([ 
    [   A1+A2, u*SQRT2 ], 
    [ v*SQRT2, a       ],
  ])
  B2 = A1-A2
  Q1,R1 = get_sub_qr(B1,mode=mode)
  Q2,R2 = get_sub_qr(B2,mode=mode)
  # index shifts based on factorization mode and input matrix parity
  d  = dq if mode == REDUCED_FACTORIZATION else dp    # mode effect
  # Slicing tricks to include last element if even parities (=0)
  sd = -d  or None 
  sp = -dp or None 
  sq = -dq or None 
  # Q and X scalar elements
  rho = np.tile(R1[ -1, -1],( d,dq))
  kap = np.tile(Q1[ -1, -1],(dp, d))
  # Q and X vector elements
  r   = np.tile(R1[:sd, -1],( 1,dq)) / SQRT2
  s   = np.tile(Q1[:sp, -1],( 1, d)) / SQRT2
  t   = np.tile(Q1[ -1,:sd],(dp, 1)) / SQRT2
  # Update Q1,R1 system
  Q1 = Q1[:sp,:sd]
  R1 = R1[:sd,:sq]
  # Q and X block matrix elements 
  Qp = 0.5*(Q1+Q2)
  Qm = 0.5*(Q1-Q2)
  Rp = 0.5*(R1+R2)
  Rm = 0.5*(R1-R2)
  Zr = np.zeros((d, R1.shape[1]))
  # Explicitly construct Q and X from the elements
  Q = np.block([
    [       Qp,       s, Qm[:,::-1]    ],
    [        t,     kap,  t[:,::-1]    ],
    [ Qm[::-1], s[::-1], Qp[::-1,::-1] ],
  ]) 
  X = np.block([
    [       Rp,       r, Rm[:,::-1]    ],
    [       Zr,     rho, Zr            ],
    [ Rm[::-1], r[::-1], Rp[::-1,::-1] ],
  ])
  # Convert numpy matrices back to arrays to comply with input object
  if type(A) is np.ndarray:
    Q,X = np.asarray(Q),np.asarray(X)
  return Q,X
