Centrosymmetric QX
==================

MATLAB code `qx.m` and Python module `qx.py` to factor a
[centrosymmetric real matrix][1] `A=JAJ` into real centrosymmetric
matrices `Q` and `X`, such that `A = QX`.  Here `J` denotes the
[reversal matrix][2], whose action reverses the ordering of rows (when
applied on the left) or columns (when applied on the right).

In the full case `Q` is [square orthogonal][3] and `X` the same size as
`A`, with a [double-cone][4] structure. In the thin case, `Q` has the
same size as `A`, with orthonormal columns, while `X` is square. The
thin factorization is only performed for matrices `A` with fewer rows
than columns.  In either case the QX factorization utilizes two standard 
[QR factorizations][5].  

This file is part of a submitted research paper.

Todo
----

  * Exceptions
  * Link for research article doi
  * `'matrix'` and `'vector'` options for `qx.m`
  * upload MATLAB code to MATLAB central
  * python version


[1]: https://en.wikipedia.org/wiki/Centrosymmetric_matrix
[2]: https://en.wikipedia.org/wiki/Exchange_matrix
[3]: https://en.wikipedia.org/wiki/Orthogonal_matrix
[4]: https://doi.org/10.1016/j.laa.2015.06.036
[5]: https://en.wikipedia.org/wiki/QR_decomposition
