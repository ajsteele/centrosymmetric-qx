format compact
clc

% Case 1: (m,n) even-even
A1 = [ -2, 3,-3,-1; ...
        2, 2, 3, 2; ...
        2, 3, 2, 2; ...
       -1,-3, 3,-2 ];

% Case 2: (m,n) odd-odd
A2 = [ 1, 2, -1; ...
      .2, 4,  5; ...
       3,-1,  3; ...
       5, 4, .2; ...
      -1, 2,  1];

% Case 3: (m,n) even-odd
A3 = [ -2 -4 -1; ... 
        2  2  2; ...  
        2  2  2; ...  
       -1 -4 -2 ];

% Case 4: (m,n) odd-even
A4 = A2([2,4],:);

A_= {A1,A2,A3,A4};
c_= {'even-even', 'odd-odd', 'even-odd','odd-even'};
pf= {'PASS','FAIL'};
sf= {'THIN','FULL'};

TOL = 1e-12;

header_string(1:74) = '='; header_string(73:74) = '\n'; 
sep_string(1:74)    = '-'; sep_string(73:74)    = '\n'; 

fprintf(header_string);
fprintf('Pass/Fail absolute tolerance: %16.0e\n\n',TOL);

for select = 0:1
  sf_str = sf{select+1};
  fprintf(sep_string);
  fprintf('\nTESTING %s FACTORIZATION\n',sf_str);
  fprintf(sep_string);
  for case_ = 1:4
    A = A_{case_};
    c = c_{case_};
    fprintf(header_string);
    fprintf('++++++++++++ Case %d %s %s ++++++++++++\n',case_,c,sf_str);
    fprintf('Timing QX factorization\n');
    try
      tic;
      if select
          [Q,X] = qx(A);
      else
          [Q,X] = qx(A,0);
      end
      toc;

      fprintf(sep_string);
      res = norm(A-Q*X);
      fprintf(' ||A-QX||_2      = %16.7e\n',res);
      r(1) = res < TOL;

      res = norm(eye(size(Q'*Q,1))-Q'*Q);
      fprintf(' ||Q^T Q - I||_2 = %16.7e\n',res);
      r(2) = res < TOL;
      
      res = norm(A-A(end:-1:1,end:-1:1));
      fprintf(' ||A-JAJ||_2     = %16.7e\n',res);
      r(3) = res < TOL;

      res = norm(Q-Q(end:-1:1,end:-1:1));
      fprintf(' ||Q -JQJ||_2    = %16.7e\n',res);
      r(4) = res < TOL;

      res = norm(X-X(end:-1:1,end:-1:1));
      fprintf(' ||X -JXJ||_2    = %16.7e\n',res);
      r(5) = res < TOL;

      fprintf(sep_string);
      if all(r)
        fprintf('++++++++ CASE %d %s %s %s ++++++++\n',case_,c,sf_str,'PASSED');
      else
        fprintf('!!!!!!!! CASE %d %s %s %s !!!!!!!!\n',case_,c,sf_str,'FAILED');
      end
    catch Exception
      disp(Exception)
    end
  end 
end
