format compact
clc

A_= centrocell;
c_= {'even-even', 'odd-odd', 'even-odd','odd-even'};
pf= {'PASS','FAIL'};
sf= {'THIN','FULL'};

TOL = 1e-12;

header_string(1:74) = '='; header_string(73:74) = '\n'; 
sep_string(1:74)    = '-'; sep_string(73:74)    = '\n'; 

fprintf(header_string);
fprintf('Pass/Fail absolute tolerance: %16.0e\n\n',TOL);

for select = 0:1
  sf_str = sf{select+1};
  fprintf(sep_string);
  fprintf('\nTESTING %s FACTORIZATION\n',sf_str);
  fprintf(sep_string);
  for case_ = 1:4
    A = A_{case_};
    c = c_{case_};
    fprintf(header_string);
    fprintf('++++++++++++ Case %d %s %s ++++++++++++\n',case_,c,sf_str);
    [m,n] = size(A);
    fprintf('This matrix is %d by %d\n',m,n);
    fprintf('Timing QX factorization\n');
    try
      tic;
      if select
          [Q,X] = qx(A);
      else
          [Q,X] = qx(A,0);
      end
      toc;

      fprintf(sep_string);
      res = norm(A-Q*X);
      fprintf(' ||A-QX||_2      = %16.7e\n',res);
      r(1) = res < TOL;

      res = norm(eye(size(Q'*Q,1))-Q'*Q);
      fprintf(' ||Q^T Q - I||_2 = %16.7e\n',res);
      r(2) = res < TOL;
      
      res = norm(A-A(end:-1:1,end:-1:1));
      fprintf(' ||A-JAJ||_2     = %16.7e\n',res);
      r(3) = res < TOL;

      res = norm(Q-Q(end:-1:1,end:-1:1));
      fprintf(' ||Q-JQJ||_2     = %16.7e\n',res);
      r(4) = res < TOL;

      res = norm(X-X(end:-1:1,end:-1:1));
      fprintf(' ||X-JXJ||_2     = %16.7e\n',res);
      r(5) = res < TOL;

      fprintf(sep_string);
      if all(r)
        fprintf('++++++++ CASE %d %s %s %s ++++++++\n',case_,c,sf_str,'PASSED');
      else
        fprintf('!!!!!!!! CASE %d %s %s %s !!!!!!!!\n',case_,c,sf_str,'FAILED');
      if select
        type = 'FULL';
      else
        type = 'THIN';
      end
      %{
      filename = strcat('testfailure_case', num2str(case_), type);
      filename = char(filename);
      save(filename,'A','-v7.3');
      %}
      end
    catch Exception
      disp(Exception)
      %{
      if select
          type = 'FULL';
      else
          type = 'THIN';
      end
      msgID = getReport(Exception);
      if strcmp(msgID,'Thin factorization requires m >= n') == 0
        filename = strcat('exceptionfailure_case', num2str(case_), type);
        filename = char(filename);
        save(filename, 'A', '-v7.3');
      end
      %}
    end
  end 
end
