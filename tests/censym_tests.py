import numpy as np
from qx import qx,ReducedDimensionError,CentrosymmetryError
import censym
import itertools

array = np.array
eye   = np.eye

NUMBER_OF_RANDOM_CASES = 5
RANDOM_CASES_BASE_DIM  = 6

def mynorm(U):
  return np.abs(U).max()

# Case 1: (m,n) even-even
A1 = array([
  [ -2, 3,-3,-1 ],
  [  2, 2, 3, 2 ],
  [  2, 3, 2, 2 ],
  [ -1,-3, 3,-2 ],
])

# Case 2: (m,n) odd-odd
A2 = array([
  [  1,  2, -1 ],
  [ .2,  4,  5 ],
  [  3, -1,  3 ],
  [  5,  4, .2 ],
  [ -1,  2,  1 ],
])

# Case 3: (m,n) even-odd
A3 = array([
  [ -2, -4, -1 ], 
  [  2,  2,  2 ], 
  [  2,  2,  2 ], 
  [ -1, -4, -2 ],
])


# Case 4: (m,n) odd-even
A4 = A2[:,[0,2]]

## Collect Cases
A_ = [ A1, A2, A3, A4 ]

N = RANDOM_CASES_BASE_DIM
Shapes = [
    [N  ,N  ], 
    [N  ,N+1],
    [N+1,N  ],
    [N+1,N+1],
]
# Generate random centrosymmetric cases with the four parities
for k in range(NUMBER_OF_RANDOM_CASES):
  for sh in Shapes:
    C,S = censym.gen_random(*sh)
    A_.append(C)

TOL = 1e-12;

header_string = 72*'='+''
sep_string    = 72*'-'+''

parity = [ 'even', 'odd' ]

def tester(A):
  m,n   = A.shape
  pm,pn = m&1,n&1
  c     = '{:s}-{:s}'.format(*[ parity[p] for p in [pm,pn] ])
  R     = []
  for mode in ['reduced','complete']:
    r   = []
    print(sep_string)

    print(' A shape: ', m, n)
    try:
      Q,X = qx(A,mode=mode)
      print(' Q shape: ', *Q.shape)
      print(' X shape: ', *X.shape)

      res = mynorm(A-Q@X)
      r.append(res < TOL)
      print(' ||A-QX||_2      = {:16.7e}'.format(res) + (' PASS' if r[-1] else ' !! FAIL !!'))
                                                                                              
      QTQ = Q.T@Q                                                                             
      res = mynorm(eye(*QTQ.shape)-QTQ);                                                      
      r.append(res < TOL)                                                                     
      print(' ||Q^T Q - I||_2 = {:16.7e}'.format(res) + (' PASS' if r[-1] else ' !! FAIL !!'))
                                                                                              
      res = mynorm( A-A[::-1,::-1] );                                                         
      r.append(res < TOL)                                                                     
      print(' ||A-JAJ||_2     = {:16.7e}'.format(res) + (' PASS' if r[-1] else ' !! FAIL !!'))
                                                                                              
      res = mynorm( Q-Q[::-1,::-1] );                                                         
      r.append(res < TOL)                                                                     
      print(' ||Q -JQJ||_2    = {:16.7e}'.format(res) + (' PASS' if r[-1] else ' !! FAIL !!'))
                                                                                              
      res = mynorm( X-X[::-1,::-1] )                                                          
      r.append(res < TOL)                                                                     
      print(' ||X -JXJ||_2    = {:16.7e}'.format(res) + (' PASS' if r[-1] else ' !! FAIL !!'))
                                                                                              
##    This will always pass due to construction 
      res = np.diag(X[:len(X)//2]).min()                                                                  
      r.append(res >= 0)                                                                       
      print(' min diag X      = {:16.7e}'.format(res) + (' PASS' if r[-1] else ' !! FAIL !!'))

      if np.all(r):
        out_str = '++++++++ CASE {:>9s} {:<8s} {:<6s} ++++++++'.format(c,mode,'PASSED')
      else:                       
        out_str = '!!!!!!!! CASE {:>9s} {:<8s} {:<6s} !!!!!!!!'.format(c,mode,'FAILED')
      print(out_str)
    except ReducedDimensionError as ex:
      out_str = '!!!!!! {:<9s} {:<8s} {:<s} !!!!!!'.format(c,mode,'FAILED ReducedDimensionError Handling')
      if m < n:
        out_str = '------  {:<9s} {:<8s} {:<s}  ------'.format(c,mode,'PASSED ReducedDimensionError Handling')
      print('EXCEPTION ReducedDimensionError: ', ex)
    except CentrosymmetryError as ex:
      out_str = '!!!!!! {:<9s} {:<8s} {:<s} !!!!!!'.format(c,mode,'FAILED CentrosymmetryError')
      print('EXCEPTION CentrosymmetryError: ', ex)
    except Exception as ex:
      out_str = '!!!!!!!! CASE {:>9s} {:<8s} {:<6s} !!!!!!!!'.format(c,mode,'FAILED')
      print('Exception: ',ex)
    R.append(out_str)
  print(header_string)
  return R

print(header_string);
print('Pass/Fail absolute tolerance: {:7.0e}\n'.format(TOL))

R = []
for A in A_:
  R.append(tester(A))

print()
print('{:*^72s}'.format(' SUMMARY '))
print()
for res in list(itertools.chain(*R)):
  print('{:^72s}'.format(res))
print()
print('{:*^72s}'.format(''))
