function C = centrocell
%
% Generates a cell array of 4 centrosymmetric matrices with positive and
% negative values ranging from 0 to 1.
% These matrices have random dimensions ranging from ~1000 to ~10000
% for each dimension.
% The produced cell array has, respectively: even-even, odd-odd, even-odd,
% odd-even.
% This file is used to generate matrices for the test file.
%

C = cell(1,4);

for i = 1:4
    [m,n] = random_dim(i);
    A = centro_generator(m,n);
    C{i} = A;
end

end

function A = centro_generator(m, n)

B = rand([1 floor(.5*(m*n))]);
for i = 1:floor(.5*(m*n))
    j = rand();
    if j < .4
        B(i) = -1*B(i); 
    end
end
if mod(m*n,2) == 1
    B = [B, rand];
    B = [B, fliplr(B(1:(end-1)))];
    A = reshape(B, [m n]);
else
    B = [B, fliplr(B)];
    A = reshape(B, [m n]);
end

end

function [m,n] = random_dim(type)
    p = ceil(rand*10)*1000 + ceil(rand*10)*100 + ceil(rand*10)*10 + ceil(rand*10);
    q = ceil(rand*10)*1000 + ceil(rand*10)*100 + ceil(rand*10)*10 + ceil(rand*10);
    p = ceil(p/2);
    q = ceil(q/2);
    if p < q
        temp = q;
        q = p;
        p = temp;
    end
    if type == 1
        m = p*2;
        n = q*2;
    elseif type == 2
        m = (p*2)+1;
        n = (q*2)+1;
    elseif type == 3
        m = p*2;
        n = (q*2)+1;
    else
        m = (p*2)+1;
        n = q*2;
    end
end