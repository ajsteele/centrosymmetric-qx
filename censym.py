import numpy as np 

def decomp(A):
  """
  =====================================================================
  decomp -- Compute censym decomposition of matrix `A`.
  ---------------------------------------------------------------------

  For arbitrary rectangular matrix `A`,

    `A = ( A + J_m A J_n ) / 2 + ( A - J_m A J_n ) / 2 = C + S`

  where `J` is the exchange matrix. `C` is centrosymmetric and `S` is
  skew-centrosymmetric. 

  Parameters
  ----------
  A : array_like, float, shape (m,n)
    Arbitrary rectangular matrix

  Returns
  -------
  C : array_like, float, shape (m,n)
    Centrosymmetric part of `A`
  S : array_like, float, shape (m,n)
    Skew-centrosymmetric part of `A`

  References
  ----------
  [1]: https://en.wikipedia.org/wiki/Centrosymmetric_matrix
  [2]: https://en.wikipedia.org/wiki/Exchange_matrix

  """
  JAJ = A[::-1,::-1]
  C,S = 0.5*(A+JAJ), 0.5*(A-JAJ)
  return C,S

def gen_random(m,n=None):
  """
  =====================================================================
  gen_random -- Generates censym decomp of random matrix `A` 
  ---------------------------------------------------------------------

  Generates a centrosymmetric decomposition of a random matrix `A` with
  elements uniformly distributed between -1 and 1.

  If `n` is unspecified, a square matrix of dimension `m` is generate

  Parameters
  ----------
  m : integer
    Number of rows for resultant matrix (and columns if n is not specified)
  n : integer, optional
    Number of columns for resultant matrix

  Returns
  -------
  C : array_like, float, shape (m,n)
    Centrosymmetric part of `A`
  S : array_like, float, shape (m,n)
    Skew-centrosymmetric part of `A`

  """
  n = m if n is None else n
  R = np.random.rand(m,n)
  A = 2*(R-0.5)
  return decomp(A)
