function [Q,X] = qx(A,~)
%
%
%

THIN_FACTORIZATION = 0;
FULL_FACTORIZATION = 1;

[m,n] = size(A);
TOL = 100*eps;

full_or_thin = FULL_FACTORIZATION;
if nargin > 1
    if m < n
        error('qx:qx:ThinDimension','Thin factorization requires m >= n');
    end
    full_or_thin = THIN_FACTORIZATION;
end

if max(max(abs(A-A(end:-1:1,end:-1:1)))) > TOL
    error('qx:qx:NotCentrosymmetric','A must be centrosymmetric');
end

p1 = floor(m/2); q1 = floor(n/2);
p2 = ceil(m/2);  q2 = ceil(n/2);
p12 = p1+1:p2; q12 = q1+1:q2;
dp = p2-p1; dq = q2-q1;
A1 = A(1:p1,1:q1);
A2 = A(1:p1,q2+1:end); A2 = A2(:,end:-1:1);
u = A(1:p1,q12);
v = A(p12,1:q1);
a = A(p12,q12);
B1 = [ A1+A2 , u*sqrt(2) ; v*sqrt(2), a ];
B2 = A1-A2;
if full_or_thin == FULL_FACTORIZATION % full QRs
    [Q1,R1] = qr(B1);
    [Q2,R2] = qr(B2);
else % thin QRs
    [Q1,R1] = qr(B1,0);
    [Q2,R2] = qr(B2,0);
end
% guarantee positive (anti-)diagonal coefs in X
[r1m,r1n] = size(R1);
mask = R1(1:r1n+1:end)<0;
Q1(:,mask) = -Q1(:,mask);
R1(mask,:) = -R1(mask,:);
[r2m,r2n] = size(R2);
mask = R2(2:r2n+1:end)<0;
Q2(:,mask) = -Q2(:,mask);
R2(mask,:) = -R2(mask,:);
if full_or_thin == FULL_FACTORIZATION
    d = dp;
else
    d = dq;
end
% Build Q and X
r   = repmat(R1(1:end-d,end),1,dq)/sqrt(2);
s   = repmat(Q1(1:end-dp,end),1,d )/sqrt(2);
t   = repmat(Q1(end,1:end-d),dp,1)/sqrt(2);
rho = repmat(R1(end,end),d,dq);
kap = repmat(Q1(end,end),dp,d);
Q1 = Q1(1:end-dp,1:end-d);
R1 = R1(1:end-d,1:end-dq);
Qp = (Q1+Q2)/2;
Qm = (Q1-Q2)/2;
Rp = (R1+R2)/2;
Rm = (R1-R2)/2;
Zr = zeros(d,size(R1,2));
%
Q = [ Qp,  s, Qm(:,end:-1:1);  ...
       t,kap, t(:,end:-1:1);   ...
      Qm(end:-1:1,:), s(end:-1:1), Qp(end:-1:1,end:-1:1) ];
X = [ Rp,  r, Rm(:,end:-1:1);  ...
      Zr,rho, Zr;              ...
    Rm(end:-1:1,:), r(end:-1:1), Rp(end:-1:1,end:-1:1) ];
end
